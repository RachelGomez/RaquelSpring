package com.raquelgomez.springpractica.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String nom;
    private String cognoms;
    private String NIF;
    private Date dataNaixement;
    private String email;
    private String telefon;
    private Long biblioteca;

    public Client(){}

    public Client(String nom, String cognoms, String NIF, Date dataNaixement, String email, String telefon, Long biblioteca) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.NIF = NIF;
        this.dataNaixement = dataNaixement;
        this.email = email;
        this.telefon = telefon;
        this.biblioteca = biblioteca;
    }

    public String getNom() {
        return nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public String getNIF() {
        return NIF;
    }

    public Date getDataNaixement() {
        return dataNaixement;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefon() {
        return telefon;
    }

    public Long getBiblioteca() {
        return biblioteca;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    public void setDataNaixement(Date dataNaixement) {
        this.dataNaixement = dataNaixement;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public void setBiblioteca(Long biblioteca) {
        this.biblioteca = biblioteca;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", cognoms='" + cognoms + '\'' +
                ", NIF='" + NIF + '\'' +
                ", dataNaixement=" + dataNaixement +
                ", email='" + email + '\'' +
                ", telefon='" + telefon + '\'' +
                ", biblioteca=" + biblioteca +
                '}';
    }
}


