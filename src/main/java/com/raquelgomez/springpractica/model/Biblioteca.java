package com.raquelgomez.springpractica.model;

public class Biblioteca {
    private String nom;
    private String adreca;
    private String poblacio;

    public Biblioteca(String nom, String adreca, String poblacio) {
        this.nom = nom;
        this.adreca = adreca;
        this.poblacio = poblacio;
    }
    public Biblioteca(){}

    public String getNom() {
        return nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public String getPoblacio() {
        return poblacio;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public void setPoblacio(String poblacio) {
        this.poblacio = poblacio;
    }
}
