package com.raquelgomez.springpractica;

import javafx.application.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.raquelgomez.springpractica.model.Client;
import com.raquelgomez.springpractica.repository.ClientRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
@EnableJpaRepositories({"com.raquelgomez.springpractica.model","com.raquelgomez.springpractica.repository"})

public class SpringpracticaApplication {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	public static void main(String[] args) {
		SpringApplication.run(SpringpracticaApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(ClientRepository repository) {
		return (args) -> {
			SimpleDateFormat format=new SimpleDateFormat("yyyy-mm-dd");
			Date date=null;

			// save a couple of customers
			repository.save(new Client("Jack", "Bauer","45784852E",format.parse("23-02-1992"),"jackbauer@gmail.com","003478585555",1L));


			// fetch all clients
			log.info("Clients found with findAll():");
			log.info("-------------------------------");
			for (Client client : repository.findAll()) {
				log.info(client.toString());
			}
			log.info("");
		};
	}
}