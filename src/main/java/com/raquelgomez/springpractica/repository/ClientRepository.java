package com.raquelgomez.springpractica.repository;

import com.raquelgomez.springpractica.SpringpracticaApplication;
import com.raquelgomez.springpractica.model.Client;
import org.springframework.data.repository.CrudRepository;
import java.util.Date;
import java.util.List;

public interface ClientRepository extends CrudRepository<Client, Long> {
    Client save(Client client);
    List<Client>findAll();

}
